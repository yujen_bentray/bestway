function addSize(item){
    let classSizesList=['gallery__img--small','gallery__img--large','gallery__img--medium'];
    let classListLength=classSizesList.length;
    let randomIndex=Math.floor((Math.random() * classListLength));
    item.classList.add(classSizesList[randomIndex]);
  }
  let galeryImgList=document.querySelectorAll('.gallery .gallery__img');
  galeryImgList.forEach(addSize);


function readMore(el){

  console.log("dfd",el.previousElementSibling);
  el.previousElementSibling.classList.toggle("truncate");
  if(el.innerHTML==="Read Less"){
    el.innerHTML="Read More";
  }
  else{
    el.innerHTML="Read Less";
  }
}


  new WOW({
    boxClass:     'wow',      // default
    animateClass: 'animated', // default
    offset:       0,          // default
    mobile:       true,       // default
    live:         true        // default
  }).init();
  


  // counter section js--------------------------------------------------------------------------------------------
var a = 0;
$(window).scroll(function () {

    var oTop = $('#counter').offset().top - window.innerHeight;
    if (a == 0 && $(window).scrollTop() > oTop) {
        $('.counter-value').each(function () {
            var $this = $(this),
                countTo = $this.attr('data-count');
            $({
                countNum: $this.text()
            }).animate({
                    countNum: countTo
                },

                {

                    duration: 2000,
                    easing: 'swing',
                    step: function () {
                        $this.text(Math.floor(this.countNum));
                    },
                    complete: function () {
                        $this.text(this.countNum);
                        //alert('finished');
                    }

                });
        });
        a = 1;
    }

});


var clientSwiper = new Swiper(".clients__swiper", {
    spaceBetween: 50,
    slidesPerView: 5,
    pagination: {
        el: ".swiper-pagination",
        clickable: true,
    },
    breakpoints: {
      0: {
        slidesPerView: 1,
      },
      640: {
        slidesPerView: 2,
      },
      768: {
        slidesPerView: 2,
      },
      1024: {
        slidesPerView: 2,
      },
      1440:{
        slidesPerView: 3,
      }
    },
});


var jobsSwiper = new Swiper(".jobs__swiper", {
  slidesPerView: 4,
  spaceBetween: 15,
  pagination: {
    el: ".swiper-pagination",
    clickable: true,
  },
  navigation: {
    nextEl: ".jobs__swiper__btns__next",
    prevEl: ".jobs__swiper__btns__prev",
  },
  breakpoints: {
    0: {
      slidesPerView: 1,
    },
    640: {
      slidesPerView: 2,
    },
    768: {
      slidesPerView: 2,
    },
    1024: {
      slidesPerView: 3,
    },
    1440:{
      slidesPerView: 4,
    }
  },
});





var blogsSwiper1 = new Swiper(".news__swiper", {
    spaceBetween:32,
    pagination: {
        el: ".swiper-pagination",
        clickable: true,
    },
    breakpoints: {
       
        741: {
          slidesPerView: 1,
        },
        1180: {
          slidesPerView: 2,
        },
        1440: {
          slidesPerView: 2,
        },
      },
});
var blogsSwiper = new Swiper(".card__sec", {
  spaceBetween:32,
  pagination: {
      el: ".swiper-pagination",
      clickable: true,
  },
  breakpoints: {
     
      741: {
        slidesPerView: 1,
      },
      1180: {
        slidesPerView: 2,
      },
      1440: {
        slidesPerView: 2,
      },
    },
});

var swiper1 = new Swiper(".hero", {
    navigation: {
      nextEl: ".ph-caret-right",
      prevEl: ".ph-caret-left",
    },
  });

